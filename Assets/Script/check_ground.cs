using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class check_ground : MonoBehaviour
{
    public player player_;
    // Start is called before the first frame update
    void Start()
    {
        
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.isTrigger == false && !collision.CompareTag("Player"))
        {
            player_.isGround = true;
            player_.isCanJump = true;
            player_.extraJump = 1;
            player_.player_anim.Play("player_idle");
            player_.player_rb.gravityScale = 1;
            player_.isAttacking = false;
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if(collision.isTrigger == false && !collision.CompareTag("Player"))
        {
            player_.isGround = false;
            player_.isAttacking = false;
/*            player_.player_anim.Play("player_jump");*/
        }
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
