using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class player : MonoBehaviour
{
    public float speed, jumpow, maxJumpow, maxSpeed, delayCombo, delayComboCount, attackRange, dashpow;
    public int extraJump, attack_page = 0;
    public bool isRight, isGround, isCanJump, isAttacking, isAble2Move;
    public GameObject player_oj;
    public GameObject player_sprite;
    public GameObject player_attack_point;
    public GameObject hold_attack_eff;
    public GameObject jumpEff;
    public GameObject jumpPos;

    public player_anim player_anim_;

    public Rigidbody2D player_rb;
    public Animator player_anim;

    public LayerMask enemy_layer;

    public int dmg;

    // Start is called before the first frame update
    void player_move()
    {
        if ((Input.GetKeyDown(KeyCode.A) || Input.GetKey(KeyCode.A)) && isAble2Move)
        {
            if(isRight == true)
            {
                isRight = false;
                player_oj.transform.Rotate(0f, 180f, 0f);
            }
            player_rb.AddForce(Vector2.left * speed);
            if(isGround && !isAttacking)
            {
                player_anim.Play("player_move");
            }
        }
        if((Input.GetKeyUp(KeyCode.A) || Input.GetKeyUp(KeyCode.D)) && player_rb.velocity.x != 0 && isGround)
        {
            player_rb.velocity = new Vector2(0, 0);
        }
        if((Input.GetKeyDown(KeyCode.D) || Input.GetKey(KeyCode.D)) && isAble2Move)
        {
            if(isRight == false)
            {
                isRight = true;
                player_oj.transform.Rotate(0f, 180f, 0f);
            }
            player_rb.AddForce(Vector2.right * speed);
            if(isGround && !isAttacking)
            {
                player_anim.Play("player_move");
            }
        }

        if(Input.GetKey(KeyCode.Space) && isCanJump)
        {
            if(player_rb.velocity.y > maxJumpow)
            {
                isCanJump = false;
            }
            if(isCanJump)
            {
                player_rb.AddForce(Vector2.up * jumpow);
            }
        }
        if (Input.GetKeyUp(KeyCode.Space))
        {
            isCanJump = false;
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            isGround = false;
            player_anim.Play("player_jump");
            if (!isGround && !isCanJump && extraJump > 0)
            {
                Instantiate(jumpEff, jumpPos.transform.position, jumpPos.transform.rotation);
                extraJump--;
                player_rb.gravityScale = 1;
                player_rb.velocity = new Vector2(player_rb.velocity.x, 0);
                player_rb.AddForce(Vector2.up * jumpow * 3);
                player_anim.Play("player_air_jump");
            }
        }
        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            player_sprite.SetActive(false);
            isCanJump = false;
            isAble2Move = false;
            StartCoroutine(wait2DashEnd());
            if (isRight) { player_rb.AddForce(Vector2.right * dashpow) ; }
            else { player_rb.AddForce(Vector2.left * dashpow) ; }
            if (!isGround) { /*player_rb.gravityScale = 0*/; }
        }
        if (player_rb.velocity.x >= maxSpeed)
        {
            player_rb.velocity = new Vector2(maxSpeed,player_rb.velocity.y);
        }
        if(player_rb.velocity.x <= -maxSpeed)
        {
            player_rb.velocity = new Vector2(-maxSpeed, player_rb.velocity.y);
        }
        if(player_rb.velocity.x == 0 && isGround && !isAttacking && player_sprite.activeSelf)
        {
            player_anim.Play("player_idle");
        }
        if(!isGround)
        {
            player_rb.gravityScale += 0.05f;
        }
    }
    IEnumerator wait2DashEnd()
    {
        yield return new WaitForSeconds(0.05f);
        yield return 0;
        player_rb.velocity = new Vector2(0, 0);
        isCanJump = true;
        isAble2Move = true;
        player_sprite.SetActive(true);
        player_anim.Play("player_idle");
/*        player_rb.gravityScale = 1;*/
    }
    void player_normal_attack()
    {
        if(Input.GetKeyDown(KeyCode.Keypad1) && !isAttacking)
        {
            if(delayComboCount > 0 && attack_page < 3)
            {
                attack_page++;
                delayComboCount = delayCombo;
            }

            if(isGround)
            {
                player_rb.velocity = new Vector2(0, 0);
                if (isRight)
                {
                    player_rb.velocity = new Vector2(1, 0);
                }
                else
                {
                    player_rb.velocity = new Vector2(-1, 0);
                }
            }
            switch(attack_page)
            {
                case 1 : player_anim.Play("player_attack_1"); break;
                case 2 : player_anim.Play("player_attack_2"); break;
                case 3 : player_anim.Play("player_attack_3"); break;
            }
            isAttacking = true;
        }
        if ((attack_page > 0) && !isAttacking)
        {
            delayComboCount -= Time.deltaTime;
            if (delayComboCount <= 0)
            {
                attack_page = 0;
                delayComboCount = delayCombo;
            }
        }
    }
    void player_special_attack_1()
    {
        if(Input.GetKey(KeyCode.Keypad2))
        {
            isAttacking = true;
            player_anim_.hold_attack();
        }
        if(Input.GetKeyUp(KeyCode.Keypad2) && player_anim_.specAttackPage > 0)
        {
            player_anim_.hold_attack_slash();
        }
    }
    void attack_dmg()
    {
        Collider2D[] dmgEnemies = Physics2D.OverlapCircleAll(player_attack_point.transform.position, attackRange, enemy_layer);
        Debug.Log("enemylist.length = " + dmgEnemies.Length);
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(player_attack_point.transform.position, attackRange);
    }
    void Start()
    {
        player_anim.Play("player_idle");
        isAble2Move = true;
    }
    
    // Update is called once per frame
    void Update()
    {
        
    }
    private void FixedUpdate()
    {
        player_move();
        player_normal_attack();
        player_special_attack_1();
/*        attack_dmg();*/
    }
}
