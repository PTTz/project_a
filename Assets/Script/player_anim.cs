using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class player_anim : MonoBehaviour
{
    public player player_;
    public GameObject hit_light;
    public GameObject attack_point;
    public GameObject hold_attack_point;
    public GameObject hold_attack_eff;

    public float specTime, specTimeCount;
    public int specAttackPage;

    // Start is called before the first frame update
    void Start()
    {
        
    }
    void normal_attack()
    {
        Collider2D[] dmgEnemies = Physics2D.OverlapCircleAll(attack_point.transform.position, player_.attackRange, player_.enemy_layer);
        if(dmgEnemies.Length > 0)
        {
            if (player_.isRight) 
            {
                Instantiate(hit_light, attack_point.transform.position, Quaternion.Euler(0, 90, 0));
            } 
            else 
            {
                Instantiate(hit_light, attack_point.transform.position, Quaternion.Euler(180, 90, 0));
            }
        }
    }
    public void hold_attack()
    {
        hold_attack_eff.SetActive(true);
        specTimeCount -= Time.deltaTime;
        if(specTimeCount < 0)
        {
            specTimeCount = specTime;
            specAttackPage += 1;
            switch(specAttackPage)
            {
                case 0: player_.player_anim.Play("player_hold_attack"); break;
                case 1: { toSpecPageN(-0.448f, -0.064f, 0.1f, 0.35f) ; player_.player_anim.Play("player_hold_attack") ; }; break;
                case 2: toSpecPageN(-0.61f, -0.041f, 0.12f, 0.5f); break;
                case 3: { toSpecPageN(-0.816f, -0.011f, 0.14f, 0.7f) ; player_.player_anim.Play("player_hold_attack_p3"); }; break;
            }
        }
    }
    public void hold_attack_slash()
    {
        LeanTween.rotate(hold_attack_point, new Vector3(0f,0f,180f), 0.5f);
    }
    void toSpecPageN(float posX, float posY, float rotaX, float rotaY)
    {
        LeanTween.scale(hold_attack_eff, new Vector3(rotaX,rotaY,1f), 0.1f);
        LeanTween.moveLocal(hold_attack_eff, new Vector3(posX, posY, 1f), 0.1f);
        /*        LeanTween.moveLocal(hold_attack_eff, new Vector3(2f, 2f, 2f), 0.1f);*/
    }
    void end_attack()
    {
        player_.isAttacking = false;
    }
    void end_combo_attack()
    {
        player_.attack_page = 0;
        player_.isAttacking = false;
    }

    // Update is called once per frame
    void Update()
    {

    }
}
